function countLetter(letter, sentence) {
    let result = 0;
    console.log(result)

    if (letter.length === 1) {
        for(let i = 0; i < sentence.length; i++) {
            if (sentence[i] === letter) {
                result++;
            }
        }
    } else {
        result = undefined

    }

      return result
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    let word = text.toLowerCase();

    let arr = [];

    for(let i = 0;, i < word.length;, i++;) {
        if (arr.indexOf(text[i]) !== -1) {
            return false
        } else {
            arr.push(text[i]);
        }
    }

    return true
    // If the function finds a repeating letter, return false. Otherwise, return true.
    

}

function purchase(age, price) {
    let normalPrice = price.toFixed(2);
     let discountedPrice = price * 0.8;
    // Return undefined for people aged below 13.
    if (age < 13) {

        return undefined
    } else if ( age <= 21){
        let newPrice = discountedPrice.toFixed(2)
        return newPrice.toString()
    } else if (age >= 65 ) {
        let newPrice = discountedPrice.toFixed(2)
        return newPrice.toString()
    } else {
        return normalPrice.toString()
    }
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
}

function findHotCategories(items) {
    let itemArray = [];
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.
    
    items.map((item) => {
        if (item.stocks == 0 && !itemArray.includes(item.category)) {
            itemArray.push(item.category)
        }
    })

    return itemArray;
    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.
    let flyingVoters = [];

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']
    
    candidateA.map((candidates) => {
        if (candidateB.includes(candidates)) {
           flyingVoters.push(candidates)
        }
    })

    return flyingVoters
    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};